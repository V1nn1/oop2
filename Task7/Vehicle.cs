﻿using System;

namespace Task7
{
    public class Vehicle
    {
        protected string _coordinates;
        protected string _price;
        protected string _yearP;

        public Vehicle(string coordinates, string price, string yearP)
        {
            _coordinates = coordinates;
            _price = price;
            _yearP = yearP;
        }

        public virtual void ShowInfo(Vehicle vehicle)
        {
            Console.Write($"Type - {vehicle.GetType().Name}, coordinates - {_coordinates}, price - {_price}, production year - {_yearP}");
        }
    }
}
