﻿using System;

namespace Task7
{
    public class Plane : Vehicle
    {
        protected string _height;
        protected string _capacity;

        public Plane(string coordinates, string price, string yearP, string height, string capacity)
            : base(coordinates, price, yearP)
        {
            _height = height;
            _capacity = capacity;
        }

        public override void ShowInfo(Vehicle vehicle)
        {
            Console.WriteLine();
            base.ShowInfo(vehicle);
            Console.Write($", height - {_height}, capacity - {_capacity}");
        }
    }
}
