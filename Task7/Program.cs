﻿using System;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {

            Vehicle[] _vehicles = new Vehicle[3] { new Car("1200", "1500", "2020"), new Plane("1500", "1800", "2019", "10", "500"), new Ship("1500", "1800", "2001", "20", "700") };

            //довелось тут робити через метод, а не WriteLine, т.я. не знав, як додати проперті, якщо вони є в похідному, але не існують в базовому 
            //наприклад _capacity в Plane. Потрібно було додати його і в базовий, щоб мати доступ через _vehicles[j].Capacity?
            for (int j = 0; j < 3; j++)
            {
                _vehicles[j].ShowInfo(_vehicles[j]);
            }

            Console.ReadKey();
        }
    }
}
