﻿using System;

namespace Task7
{
    public class Ship : Vehicle
    {
        protected string _port;
        protected string _capacity;

        public Ship(string coordinates, string price, string yearP, string port, string capacity)
            : base(coordinates, price, yearP)
        {
            _port = port;
            _capacity = capacity;
        }

        public override void ShowInfo(Vehicle vehicle)
        {
            Console.WriteLine();
            base.ShowInfo(vehicle);
            Console.Write($", port - {_port}, capacity - {_capacity}");
        }
    }
}
