﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Converter convert = new Converter(28.25, 33.55, 0.3);
                Console.WriteLine(convert.ConvertCurrency("uah", "usd", 2800));
                Console.WriteLine(convert.ConvertCurrency("eur", "uah", 1000));
                Console.WriteLine(convert.ConvertCurrency("uah", "uah", 1000));
                Console.WriteLine(convert.ConvertCurrency("eur", "usd", 1000));
            }
            catch (ArgumentException message)
            {
                Console.WriteLine(message.Message);
            }

            Console.ReadKey();
        }
    }
}
