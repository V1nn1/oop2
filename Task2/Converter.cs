﻿using System;

namespace Task2
{
    public class Converter
    {
        private double _usdCourse;
        private double _eurCourse;
        private double _rubCourse;

        public Converter(double usd, double eur, double rub)
        {
            _usdCourse = usd;
            _eurCourse = eur;
            _rubCourse = rub;
        }

        public double ConvertCurrency(string from, string to, double amount)
        {
            double result = 0;
            if (from == "uah")
            {
                switch (to)
                {
                    case "usd":
                        result = amount / _usdCourse;
                        break;
                    case "eur":
                        result = amount / _eurCourse;
                        break;
                    case "rub":
                        result = amount / _rubCourse;
                        break;
                    case "uah":
                        result = amount;
                        break;
                }
                return result;
            }
            else if (to == "uah")
            {
                switch (from)
                {
                    case "usd":
                        result = amount * _usdCourse;
                        break;
                    case "eur":
                        result = amount * _eurCourse;
                        break;
                    case "rub":
                        result = amount * _rubCourse;
                        break;
                    case "uah":
                        result = amount;
                        break;
                }
                return result;
            }
            else
            {
                throw new ArgumentException("Invalid converter parameters");
            }
        }
    }
}
