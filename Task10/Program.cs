﻿using System;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            IAbstractHandler handler = null;
            bool _success = false;
            Console.WriteLine("Please, enter the type of document - XML, DOC or TXT:");
            while (!_success)
            {
                switch (Console.ReadLine().ToUpper())
                {
                    case "XML":
                        handler = new XMLHandler();
                        _success = true;
                        break;
                    case "DOC":
                        handler = new DOCHandler();
                        _success = true;
                        break;
                    case "TXT":
                        handler = new TXTHandler();
                        _success = true;
                        break;
                    default:
                        Console.WriteLine("Wrong document type");
                        break;
                }
            }

            handler.Open();
            handler.Create();
            handler.Change();
            handler.Save();

            Console.ReadLine();
        }
    }
}
