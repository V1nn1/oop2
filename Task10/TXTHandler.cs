﻿using System;

namespace Task10
{
    public class TXTHandler : IAbstractHandler
    {
        public void Open()
        {
            Console.WriteLine("TXT Open");
        }
        public void Create()
        {
            Console.WriteLine("TXT Create");
        }
        public void Change()
        {
            Console.WriteLine("TXT Change");
        }
        public void Save()
        {
            Console.WriteLine("TXT Save");
        }
    }
}
