﻿using System;

namespace Task10
{
    public class XMLHandler : IAbstractHandler
    {
        public void Open()
        {
            Console.WriteLine("XML Open");
        }
        public void Create()
        {
            Console.WriteLine("XML Create");
        }
        public void Change()
        {
            Console.WriteLine("XML Change");
        }
        public void Save()
        {
            Console.WriteLine("XML Save");
        }
    }
}
