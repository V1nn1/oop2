﻿using System;

namespace Task10
{
    public class DOCHandler : IAbstractHandler
    {
        public void Open()
        {
            Console.WriteLine("DOC Open");
        }
        public void Create()
        {
            Console.WriteLine("DOC Create");
        }
        public void Change()
        {
            Console.WriteLine("DOC Change");
        }
        public void Save()
        {
            Console.WriteLine("DOC Save");
        }
    }
}
