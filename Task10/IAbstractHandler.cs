﻿namespace Task10
{
    interface IAbstractHandler
    {
        void Open();
        void Create();
        void Change();
        void Save();
    }
}
