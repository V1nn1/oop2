﻿using System;

namespace Task6
{
    public class GoodPupil : Pupil
    {
        public GoodPupil(string name, int read, int study, int write, int relax) : base(name, read, study, write, relax)
        {
        }

        public override void Read()
        {
            Console.WriteLine($"{_read} points - good reading speed");
        }
        public override void Relax()
        {
            Console.WriteLine($"{_relax} points - good relax");
        }
        public override void Study()
        {
            Console.WriteLine($"{_study} points - good study");
        }
        public override void Write()
        {
            Console.WriteLine($"{_write} points - good write");
        }

    }
}
