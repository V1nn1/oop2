﻿using System;

namespace Task6
{
    public class BadPupil : Pupil
    {
        public BadPupil(string name, int read, int study, int write, int relax) : base(name, read, study, write, relax)
        {
        }

        public override void Read()
        {
            Console.WriteLine($"{_read} points - bad reading speed");
        }
        public override void Relax()
        {
            Console.WriteLine($"{_relax} points - bad relax");
        }
        public override void Study()
        {
            Console.WriteLine($"{_study} points - bad study");
        }
        public override void Write()
        {
            Console.WriteLine($"{_write} points - bad write");
        }
    }
}
