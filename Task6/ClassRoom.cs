﻿using System;

namespace Task6
{
    public class ClassRoom
    {
        Pupil[] _pupils;

        public ClassRoom(Pupil a, Pupil b)
        {
            throw new ArgumentException("The class should consist of 4 people");
        }
        public ClassRoom(Pupil a, Pupil b, Pupil c)
        {
            throw new ArgumentException("The class should consist of 4 people");
        }
        public ClassRoom(Pupil a, Pupil b, Pupil c, Pupil d)
        {
            _pupils = new Pupil[4] { a, b, c, d };
        }

        public void ShowRating()
        {
            for (int i = 0; i < _pupils.Length; i++)
            {
                Console.WriteLine(_pupils[i].Name);
                _pupils[i].Relax();
                _pupils[i].Read();
                _pupils[i].Study();
                _pupils[i].Write();
            }
        }
    }
}
