﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ClassRoom classRoom = new ClassRoom(new BadPupil("Max", 5, 10, 8, 11), new GoodPupil("Adam", 5, 10, 8, 11), new ExcelentPupil("Alice", 12, 12, 12, 12), new Pupil("Margo", 2, 3, 4, 5));
                classRoom.ShowRating();
            }
            catch (ArgumentException error)
            {
                Console.WriteLine(error.Message);
            }
            Console.ReadKey();
        }
    }
}
