﻿using System;

namespace Task6
{
    public class ExcelentPupil : Pupil
    {
        public ExcelentPupil(string name, int read, int study, int write, int relax) : base(name, read, study, write, relax)
        {
        }

        public override void Read()
        {
            Console.WriteLine($"{_read} points - excelent reading speed");
        }
        public override void Relax()
        {
            Console.WriteLine($"{_relax} points - excelent relax");
        }
        public override void Study()
        {
            Console.WriteLine($"{_study} points - excelent study");
        }
        public override void Write()
        {
            Console.WriteLine($"{_write} points - excelent write");
        }
    }
}
