﻿using System;


namespace Task6
{
    public class Pupil
    {
        protected int _read;
        protected int _study;
        protected int _write;
        protected int _relax;
        protected string _name;

        public string Name { get { return _name; } }

        public Pupil(string name, int read, int study, int write, int relax)
        {
            _read = read;
            _study = study;
            _write = write;
            _relax = relax;
            _name = name;
        }

        public virtual void Study()
        {
            Console.WriteLine($"{_study} points - standard performance");
        }
        public virtual void Read()
        {
            Console.WriteLine($"{_read} points - standard reading speed");
        }
        public virtual void Write()
        {
            Console.WriteLine($"{_write} points - standard literacy");
        }
        public virtual void Relax()
        {
            Console.WriteLine($"{_relax} points - standard rest");
        }
    }
}
