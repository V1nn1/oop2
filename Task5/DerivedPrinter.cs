﻿using System;

namespace Task5
{
    public class DerivedPrinter : Printer
    {
        public new void Print(string value)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(value);
        }
    }
}
