﻿using System;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            var derived = new DerivedPrinter();
            derived.Print("Derived printer string...");

            var basePrinter = (Printer)derived;
            basePrinter.Print("Base printer string...");

            var derived2 = (DerivedPrinter)basePrinter;
            derived2.Print("Derived 2 printer string...");

            Console.ReadKey();
        }
    }
}
