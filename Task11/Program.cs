﻿using System;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();

            Console.WriteLine("Play or Record?");

            string command = Console.ReadLine();

            if (command == "Play")
            {
                IPlayable playable = player as IPlayable;
                playable.Play();
                playable.Pause();
                playable.Stop();
            }
            else if (command == "Record")
            {
                IRecodable recodable = player as IRecodable;
                recodable.Record();
                recodable.Pause();
                recodable.Stop();
            }
            else
            {
                Console.WriteLine("Wrong command");
            }

            Console.ReadLine();
        }
    }
}
