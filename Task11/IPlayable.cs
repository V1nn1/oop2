﻿namespace Task11
{
    interface IPlayable
    {
        void Play();
        void Pause();
        void Stop();
    }
}
