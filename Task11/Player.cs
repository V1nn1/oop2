﻿using System;

namespace Task11
{
    public class Player : IPlayable, IRecodable
    {
        void IPlayable.Play()
        {
            Console.WriteLine("Playback started");
        }
        void IPlayable.Pause()
        {
            Console.WriteLine("Playback paused");
        }
        void IPlayable.Stop()
        {
            Console.WriteLine("Playback stoped");
        }
        void IRecodable.Record()
        {
            Console.WriteLine("Record started");
        }
        void IRecodable.Pause()
        {
            Console.WriteLine("Record paused");
        }
        void IRecodable.Stop()
        {
            Console.WriteLine("Playback stoped");
        }
    }
}
