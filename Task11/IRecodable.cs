﻿namespace Task11
{
    interface IRecodable
    {
        void Record();
        void Pause();
        void Stop();
    }
}
