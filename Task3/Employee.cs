﻿namespace Task3
{
    public class Employee
    {
        private string _name;
        private string _surname;

        public string Name { get { return _name; } }
        public string Surname { get { return _surname; } }

        public Employee(string name, string surname)
        {
            _name = name;
            _surname = surname;
        }

        public void Sallary(string position, byte workYears, out double sallary, out double tax)
        {
            switch (position)
            {
                case "manager":
                    if (workYears > 2)
                    {
                        sallary = 1000;
                    }
                    else if (workYears > 5)
                    {
                        sallary = 3000;
                    }
                    else if (workYears > 10)
                    {
                        sallary = 6000;
                    }
                    else
                    {
                        sallary = 600;
                    }
                    break;
                case "senior manager":
                    if (workYears > 2)
                    {
                        sallary = 5000;
                    }
                    else if (workYears > 5)
                    {
                        sallary = 10000;
                    }
                    else if (workYears > 10)
                    {
                        sallary = 16000;
                    }
                    else
                    {
                        sallary = 3600;
                    }
                    break;
                case "big boss":
                    if (workYears > 2)
                    {
                        sallary = 50000;
                    }
                    else if (workYears > 5)
                    {
                        sallary = 100000;
                    }
                    else if (workYears > 10)
                    {
                        sallary = 160000;
                    }
                    else
                    {
                        sallary = 36000;
                    }
                    break;
                default:
                    sallary = 0;
                    break;
            }
            tax = sallary * 0.195;
            sallary -= tax;
        }
    }
}
