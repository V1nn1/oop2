﻿using System;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee employee = new Employee("Denys", "Novytskyi");
            employee.Sallary("manager", 10, out double sallary, out double tax);
            Console.WriteLine($"{employee.Name} {employee.Surname} - Sallary: {sallary},Tax: {tax}");
            Console.ReadKey();
        }
    }
}
