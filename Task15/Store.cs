﻿namespace Task15
{
    public class Store
    {
        private Article[] _articles = new Article[5];
        
        public Store()
        {
            _articles[0] = new Article("art. 1", "store 1", 100.10m);
            _articles[1] = new Article("art. 2", "store 2", 200.10m);
            _articles[2] = new Article("art. 3", "store 3", 300.10m);
            _articles[3] = new Article("art. 4", "store 4", 400.10m);
            _articles[4] = new Article("art. 5", "store 5", 500.10m);
        }
        
        public int Length { get { return _articles.Length; } }

        public string this[int index]
        {
            get
            {
                return _articles[index].Name + " " + _articles[index].Store + " " + _articles[index].Price;
            }
        }

        public string this[string index]
        {
            get
            {
                for (int i = 0; i < _articles.Length; i++)
                {
                    if (_articles[i].Name == index)
                    {
                        return _articles[i].Name + " " + _articles[i].Store + " " + _articles[i].Price;
                    }
                }

                return "No such article";
            }
        }
    }
}
