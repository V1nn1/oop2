﻿using System;

namespace Task15
{
    class Program
    {
        static void Main(string[] args)
        {
            Store store = new Store();

            for (int i = 0; i < store.Length; i++)
            {
                Console.WriteLine(store[i]);
            }

            Console.WriteLine("Enter name of article to search:");

            string search = Console.ReadLine();

            Console.WriteLine(store[search]);
            Console.ReadKey();
        }
    }
}
