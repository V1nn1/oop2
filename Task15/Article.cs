﻿namespace Task15
{
    class Article
    {
        readonly string _name;
        readonly string _store;
        readonly decimal _price;

        public Article(string name, string store, decimal price)
        {
            _name = name;
            _store = store;
            _price = price;
        }

        public string Name { get { return _name; } }
        public string Store { get { return _store; } }
        public decimal Price { get { return _price; } }
    }
}
