﻿using System;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            DocumentWorker documentWorker;

            Console.WriteLine("Enter key or leave this field blank:");

            switch (Console.ReadLine())
            {
                case "exp":
                    documentWorker = (DocumentWorker)new ExpertDocumentWorker();
                    //не зрозумів, навіщо було використовувати приведення типів? просто щоб показати, що є така можливість?
                    //можно ще було написати так - documentWorker = new ExpertDocumentWorker(); - правильно?
                    break;
                case "pro":
                    documentWorker = (DocumentWorker)new ProDocumentWorker();
                    break;
                default:
                    documentWorker = new DocumentWorker();
                    break;
            }

            documentWorker.OpenDocument();
            documentWorker.EditDocument();
            documentWorker.SaveDocument();

            Console.ReadKey();
        }
    }
}
