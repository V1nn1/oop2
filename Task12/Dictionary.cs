﻿namespace Task12
{
    public class Dictionary
    {
        private string[] valueRU = new string[5];
        private string[] valueUS = new string[5];
        private string[] valueUA = new string[5];

        public Dictionary()
        {
            valueRU[0] = "книга"; valueUS[0] = "book"; valueUA[0] = "книга";
            valueRU[1] = "ручка"; valueUS[1] = "pen"; valueUA[1] = "ручка";
            valueRU[2] = "солнце"; valueUS[2] = "sun"; valueUA[2] = "сонце";
            valueRU[3] = "яблоко"; valueUS[3] = "apple"; valueUA[3] = "яблуко";
            valueRU[4] = "стол"; valueUS[4] = "table"; valueUA[4] = "стіл";
        }

        public string this[string index]
        {
            get
            {
                for (int i = 0; i < valueRU.Length; i++)
                    if (valueRU[i] == index)
                        return valueRU[i] + " - " + valueUS[i] + " - " + valueUA[i];
                    else if (valueUA[i] == index)
                        return valueUA[i] + " - " + valueUS[i] + " - " + valueRU[i];
                    else if (valueUS[i] == index)
                        return valueUS[i] + " - " + valueUA[i] + " - " + valueRU[i];

                return string.Format("{0} - нет перевода для этого слова.", index);
            }
        }

        public string this[int index]
        {
            get
            {
                if (index >= 0 && index < valueRU.Length)
                    return valueRU[index] + " - " + valueUA[index] + " - " + valueUS[index];
                else
                    return "Попытка обращения за пределы массива.";
            }
        }
    }
}
