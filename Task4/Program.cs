﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Invoice invoice = new Invoice(100, "Customer", "Provider");
            Console.WriteLine($"{invoice.Account},{invoice.Provider},{invoice.Customer}, {invoice.Cost(300, 50, true)}");
            Console.ReadKey();
        }
    }
}
