﻿namespace Task4
{
    public class Invoice
    {
        private readonly int _account;
        private readonly string _customer;
        private readonly string _provider;
        //я так і не зрозумів, навіщо було додавати ці 2 змінні і як іх потрібно використати
        private string _article;
        private int _quantity;

        public int Account { get { return _account; } }
        public string Customer { get { return _customer; } }
        public string Provider { get { return _provider; } }

        public Invoice(int account, string customer, string provider)
        {
            _account = account;
            _customer = customer;
            _provider = provider;
        }

        public double Cost(int article, int quantity, bool needVAT)
        {
            double price;
            switch (article)
            {
                case 100:
                    if (quantity > 100)
                        price = 200;
                    else
                        price = 300;
                    break;
                case 300:
                    if (quantity > 50)
                        price = 2000;
                    else
                        price = 3000;
                    break;
                case 400:
                    if (quantity > 10)
                        price = 2;
                    else
                        price = 3;
                    break;
                case 500:
                    if (quantity > 1000)
                        price = 500;
                    else
                        price = 3000;
                    break;
                default:
                    price = 300;
                    break;
            }
            if (needVAT)
                return price * quantity * 1.2;
            else
                return price * quantity;
        }
    }
}
