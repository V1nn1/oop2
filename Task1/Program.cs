﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User();
            user.Login = "Login";
            user.Name = "Name";
            user.Surname = "Surname";
            user.Age = "Age";

            Console.WriteLine($"{user.Login}, {user.Name}, {user.Surname}, {user.Age}, {user.CreateDate}");
            Console.ReadKey();
        }
    }
}
