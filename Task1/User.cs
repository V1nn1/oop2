﻿using System;

namespace Task1
{
    public class User
    {
        private readonly DateTime _createDate;

        public string Login { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }
        public string Age { set; get; }
        public DateTime CreateDate { get { return _createDate; } }

        public User()
        {
            _createDate = DateTime.Now;
        }

    }
}
