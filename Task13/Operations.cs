﻿using System;

namespace Task13
{
    class Array
    {
        private readonly int[] array;
        public Array(int n)
        {
            array = new int[n];
            Random random = new Random();

            for (int i = 0; i < n; i++)
            {
                array[i] = random.Next(-1000, 1000);
            }
        }

        public int GetMinElement()
        {
            int min = array[0];

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] < min)
                {
                    min = array[i];
                }
            }

            return min;
        }

        public int GetMaxElement()
        {
            int max = array[0];

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
            }

            return max;
        }

        public int GetSumElements()
        {
            int sum = array[0];

            for (int i = 1; i < array.Length; i++)
            {
                sum = sum + array[i];
            }

            return sum;
        }

        public string GetOddElements()
        {
            string odd = "";

            for (int i = 0; i < array.Length; i++)
            {
                if (Math.Abs(array[i] % 2) == 1)
                {
                    odd = odd + array[i] + " ";
                }
            }
            return odd;
        }

        public double GetAvgElements()
        {
            double avg = (double)GetSumElements()/array.Length;
            return avg;
        }
    }
}
