﻿using System;

namespace Task13
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            while (n < 1)
            {
                Console.WriteLine("Please enter the number of elements in the array");
                if (!int.TryParse(Console.ReadLine(), out n))
                {
                    Console.WriteLine("The value should be an integer");
                }
            }

            Array operations = new Array(n);

            Console.WriteLine($"Min - {operations.GetMinElement()}, Max - {operations.GetMaxElement()}, Sum - {operations.GetSumElements()}, Avg - {operations.GetAvgElements()}, Odds - {operations.GetOddElements()}");

            Console.ReadKey();
        }
    }
}
