﻿using System;

namespace Task14
{
    class MyMatrix
    {
        private string[,] matrix;

        public MyMatrix(int rows, int col)
        {
            matrix = new string[rows, col];
            Fill();
        }

        private void Fill()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int k = 0; k < matrix.GetLength(1); k++)
                {
                    matrix[i, k] = "x";
                }
            }
        }

        public void ResizeMatrix(int rows, int col)
        {
            matrix = new string[rows, col];
            Fill();
        }

        public void PrintMyMatrix()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int k = 0; k < matrix.GetLength(1); k++)
                {
                    Console.Write(matrix[i, k]);
                }
                Console.WriteLine();
            }
        }

    }
}
