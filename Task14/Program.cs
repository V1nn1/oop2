﻿using System;

namespace Task14
{
    class Program
    {
        static void Main(string[] args)
        {
            MyMatrix matrix = new MyMatrix(5, 10);

            matrix.PrintMyMatrix();
            matrix.ResizeMatrix(10, 20);
            Console.WriteLine();
            matrix.PrintMyMatrix();

            Console.ReadLine();
        }
    }
}
